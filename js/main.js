import { 
    getTaskDone, 
    getTaskFromLocalStorage, 
    getValueFrom, 
    renderListTasks, 
    renderTasksDone 
} from "./controller.js";

// Render tasks localStorage
let tasks = getTaskFromLocalStorage();
renderListTasks(tasks);

// render tasks done
let tasksDone = getTaskDone();
renderTasksDone(tasksDone);

// Add tasks
document.getElementById("addItem").addEventListener("click", () => {
    let value = document.getElementById("newTask")
    let data = getValueFrom();

    if(data.name == "") {
        alert("Bạn chưa nhập nhiệm vụ !")
        return;
    } else {
        tasks.push(data);
        localStorage.setItem("TASK", JSON.stringify(tasks));
        renderListTasks(tasks);
        value.value = "";
        value.focus();
    };
});

// Delete tasks
const deleteTask = (id) => {
    tasks.splice(id, 1);
    localStorage.setItem("TASK", JSON.stringify(tasks));
    renderListTasks(tasks);
};
window.deleteTask = deleteTask;

// Tick task done
const tickTaskDone = (id) => {
    let tasks = getTaskFromLocalStorage();
    let indexTask = tasks.findIndex((task, index) => {
        return index == id;
    });
    if(indexTask == -1) {
        alert("Errol !");
    }

    deleteTask(id);
    tasks = tasks[indexTask]
    tasksDone.push(tasks);
    localStorage.setItem("DONE", JSON.stringify(tasksDone));
    renderTasksDone(tasksDone); 
}
window.tickTaskDone = tickTaskDone;

// Delete task done
const deleteTaskDone = (id) => {
    tasksDone.splice(id, 1);
    localStorage.setItem("DONE", JSON.stringify(tasksDone));
    renderTasksDone(tasksDone);
};
window.deleteTaskDone = deleteTaskDone;

// Discard task done
const discardTaskDone = (id) => {
    let tasksDone = getTaskDone();
    let indexTask = tasksDone.findIndex((task, index) => {
        return index == id;
    });
    if(indexTask == -1) {
        alert("Errol !");
    };

    deleteTaskDone(id);
    tasksDone = tasksDone[indexTask];
    tasks.push(tasksDone);
    localStorage.setItem("TASK", JSON.stringify(tasks));
    renderListTasks(tasks);
};
window.discardTaskDone = discardTaskDone;

// Render a - z
document.getElementById("two").addEventListener("click", () => {
    tasks.sort((a, b) => {
        return a.name.localeCompare(b.name);
    });
    renderListTasks(tasks);

    tasksDone.sort((a, b) => {
        return a.name.localeCompare(b.name);
    });
    renderTasksDone(tasksDone);
});

// Render z - a
document.getElementById("three").addEventListener("click", () => {
    tasks.sort((a, b) => {
        return b.name.localeCompare(a.name);
    });
    renderListTasks(tasks);

    tasksDone.sort((a, b) => {
        return b.name.localeCompare(a.name);
    });
    renderTasksDone(tasksDone);
});