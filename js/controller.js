// Get value from
export let getValueFrom = () => {
    let value = document.getElementById("newTask").value;
    return {
        name: value,
    };
};

// Render task 
export let renderListTasks = tasks => {
    let contentHTML = "";

    tasks.map((task, index) => {
        contentHTML += `
            <li>
                <span>${task.name}</span>
                <div>
                    <i class="fa-solid fa-trash-can" onclick="deleteTask(${index})"></i>
                    <i class="fa-solid fa-circle-check" onclick="tickTaskDone(${index})"></i>
                </div>
            </li>
        `;
    });
    document.getElementById("todo").innerHTML = contentHTML;
};

// Render tasks done
export let renderTasksDone = tasks => {
    let contentHTML = "";
    
    tasks.map((task, index) => {
        contentHTML += `
            <li>
                <span>${task.name}</span>
                <div>
                    <i class="fa-solid fa-trash-can" onclick="deleteTaskDone(${index})"></i>
                    <i class="fa-solid fa-circle-check" onclick="discardTaskDone(${index})"></i>
                </div>
            </li>
        `;
    });
    document.getElementById("completed").innerHTML = contentHTML;
};

// localStorage add Task
export let getTaskFromLocalStorage = () => {
    return localStorage.getItem("TASK") ? JSON.parse(localStorage.getItem("TASK")) : [];
};

// localStorage Task done
export let getTaskDone = () => {
    return localStorage.getItem("DONE") ? JSON.parse(localStorage.getItem("DONE")) : [];
};